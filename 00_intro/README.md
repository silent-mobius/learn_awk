# intro

## What is AWK?
AWK is a domain-specific language (DSL) designed for text processing and typically used as a data extraction and reporting tool. Like sed and grep, it's a filter,and is a standard feature of most Unix-like operating systems.
The AWK language is a data-driven scripting language consisting of a set of actions to be taken against streams of textual data – either run directly on files or used as part of a pipeline – for purposes of extracting or transforming text, such as producing formatted reports.

- Why the name ?
    - Names of creators:
        - [Alfred Aho](https://en.wikipedia.org/wiki/Alfred_Aho) (author of egrep) 
        - [Peter J. Weinberger](https://en.wikipedia.org/wiki/Peter_J._Weinberger) (who worked on tiny relational databases),
        - [Brian Kernighan](https://en.wikipedia.org/wiki/Brian_Kernighan)
- Where is awk available?
    - Pre-installed on most UNIX variants, includeing, Mac.
    - Linux
    - Windows:
        - [Cygwin complete shell tool](https://www.cygwin.com)
        - [Unixutils from sourceforge ](https://unxutils.sourceforge.net)

- What is AWK good for ?
    - Find/manipulate text/ASCII files.
    - Select fields in CSV files.
    - Join merges between different columns/rows in text file.

- What is AWK NOT good for ?
    - Manipulate bin data
    - Not really good for web development
        - Although could be good html generator.

- What version of AWK exists?
    - Original: UNIX v7 1978
    - New AWK(also known as nAWK): Unix systemV, 1985
    - GNU AWK(also known as GAWK): Linux, 1991+

