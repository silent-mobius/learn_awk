# AWK Basics

## AWK Syntax

Due to `awk` being shell utility, it uses `'` as literal in order
 not to mis-match with shell inner capabilities.
When we run `awk`, it tries to read the file provided to it and
 seperatebetween column, while the IFS(Internal Field Separator) being the space(one or more).

```sh
awk '{print $1, $2}' names.txt
```
One can reverse the print, but change the `$2` with `$1`, meaning that AWK can manipulate the structure ASCII/text data.
e.g.

```sh
awk '{print $2, $1}' names.txt
```

if  you'll miss one of the parenthesis, AWK wil try to notify you regarding the error. 
Yet, the notification might be little bit cryptic, thus one must read the output error slowly. 
e.g 

```sh
awk 'print $2, $1}' names.txt
awk: cmd. line:1: print $2, $1}
awk: cmd. line:1: ^ syntax error
awk: cmd. line:1: print $2, $1}
awk: cmd. line:1:             ^ syntax error
```
---

# Actions, Patterns, Fields and Record. 

Whenever we work with AWK there are severl points of considiration we can address.

- Fields/Records:: space/tab seperated data.
- Pattens: Regex specified data.
- Actions: commands in awk syntax (print)


## Fields/Records
- Field: A field is a combination of one or more related characters
- Record: A record is a group of related fields.

AWK uses `$` to seperate or generate fields and records.
for example if you wush to print 3rd field you'll use `$3` with print:
```sh
awk '{print $3}' lightyears.txt 
```
The default IFS is space.
Field seperator with comma, without one letter will be one string.
but field seperator can be mannually created e.g.
```sh
awk '{print $1 " ," $2}' lightyears.txt 
```
the comma in double quotations is an 'artificial' seperator we created.

another slightly complicated example of awk that counts data would be as follows:

```sh
awk '{print NF, $0}' lightyears.txt 
```
```txt
4 To infinity and beyond!
5 Years of academy training, wasted!
12 Don't you get it?! You see the hat?! I am Mrs. Nesbitt!
7 I'm Buzz Lightyear, I come in peace.
20 How dare you open a Space Ranger's helmet on an uncharted planet! My eyeballs could've been sucked from their sockets!

```

the code above will, print all the lines in file `lightyears.txt`, while `NF` will add `Number of Fields`
in each line (in this case: $0 means all the lines.

## Pattens
Patterns are regular expression request that we can require from awk to seek out in a data file.
In Awk, patterns canbe used serveral times, thus if we wish to seek data with specific pattern, 
we could use `/regex/` to find it.

```sh
awk '/I/{print NF, $0}' lightyears.txt 
```
In example above we requere awk to list all fields that
have `I` in it and the result is as follows:

```txt
12 Don't you get it?! You see the hat?! I am Mrs. Nesbitt!
7 I'm Buzz Lightyear, I come in peace.
```

We can set pattern in manner that fits our needs. For example, 
if we need to print only lines that have only 4 fields(words), then we could require it like this:
```sh
awk '/NF==4/{print NF, $0}' lightyears.txt 
```
The  output should be :
```txt
4 To infinity and beyond!
```

## Actions

Actions are awk specific commands that we can use with awk utility.

for example, code below will print all the data in file.**Yet** the action in it would be `print` action.
```sh
awk '{print $0}' names.txt
```
Actions are applied to all the line that awk request is applicable. for example:

**if** you won't specify what to do with your data(filter it with `$1` for example), 
then that action will be done to the whole data file:

```sh
awk '{print}' lightyears.txt 
```
**if** no data file has been provided to awk, then it will try to read from  __stdin__
in which case you'll have to type manually for it to use the action.

Example:
Type next command in our teminal
```sh
awk '{print $2}' 
```
give it 3 words and press `Enter`
---

## AWK Flags

### -f read from file  (script)

### -F set field seperator

---