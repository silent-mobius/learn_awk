# Specifying field and record separators


As mentined before awk seperates fields with space
so if we run `awk '{print $2}'` and feed it with input of `1 2 3 4`
the field that we'll recieve will be `2`. by the way, we can provide
 input with different spacing like this `1         2 3   4` 
 but the out come should be the same(if you are using GnuAwk).

It all changes, when the field seperator is comma. meaning that if the input is `1,2,3,4`, the command `awk '{print $2}'` will print blank line. the by pass to this can be via flag `-F` and code itself should look like this
```sh
awk -F,'{print $2}' numbers.txt
```

the output
```txt
2
6
```

##### __Note__: Field Seperator can be anything

---

The interesting fact about `-F` param is that it is an external for awk, which begs the question: can we specify the field seperator inside the code?

to which the answer is _YES_. The possibility for it can be achieved via `FS` option in awk script as follows:

```sh
awk '{FS=","; print $2}' numbers.txt
```
##### __Note__: it won't work from start, due to `;` sign. it mean end of the statement, like in `C/C++/Java`,__Yet__ until awk reads FS="," and changes the filed seperator, the default is still space. Thus first line will not appear.

the output looks like:
```txt

6
```
So, how can we fix this?
just use `BEGIN` __pattern__ (as in regex) to by pass the `;` sign

```sh
awk  'BEGIN{FS=","} {print $2}' numbers.txt
``

which should have next output to provide:

```txt
2
6
```

All and all, everything is file whil you can work with data that has infinite line seperated with commas, which could be perfect, yet in real world data usually is seperated in variabous ways, and this part i am talking about _records_.

awk seperates fields byt using, spaces by default, and seperates records by using new lines. So next time you'll try to define an awk script that needs to seperate fields and recores, this is what is should look like:

```sh
awk  'BEGIN{RS="!";FS=","} {print $2}' record_numbers.txt 
```

and guess what output is:

```sh
2
5
8
11
```
---

Up until now we covered only inputing data and processing it with awk. But there is also and output part of it, and this where `OFS` and `ORS` special variables come in handy.
`OFS`  stands for Output Field Seperator, while `ORS` stands for Output Record Seperator. they can take an data and scruture it to be displayed in certain way. For Example:

in previuous chapter we talked about names.txt file and how we can construct it in some way, we can convert to a types of data that easier for awk to process.

```sh
awk  'BEGIN{OFS=", ";ORS="!"} {print $2, $1}' names.txt 

```

provides processed data as a single line

```txt
Schapelle, Alex!Sapir, Michael!Meyer, Mark!West, Serge!Schapelle, Sharon!Sapir, Karen!Meyer, Lily!West, Love!
```



---
## Recap

- Field: by default, in awk, each space devided data is _field_.
- Record: by default, in awk, each line is a _record_.
- FS: Field Seperator in awk script.
- RS: Record Seperator in awk script.
- BEGIN: special pattern recognizing the start of the input `stdin`.
- OFS: Output Field Seperator, used to format the output of the script.
- ORS: Output Record Seperator, used to format the output of the script.



---

