- create an awk script that uses the `nameemailavg.csv` file as input, and provides output that is tab seperated




<!-- 
opt1:
awk -F, '{print $1 "\t"  $2 "\t"  $3}' nameemailavg.csv

opt2:
awk 'BEGIN{FS=","; OFS="\t"} {print $1,$2,$3}' nameemailavg.csv
-->